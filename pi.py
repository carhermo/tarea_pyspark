from pyspark import SparkContext, SparkConf
import random
import pandas
import timeit
import os

N_CORES_MAX = int(os.popen("nproc").read())
N_SAMPLES_MAX = 10000000
N_SAMPLES_MIN = 1000000
print(N_CORES_MAX)

def inside(p):
    x, y = random.random(), random.random()
    return x*x + y*y < 1

def code(cores, samples):
    sc = SparkContext(master="local["+str(cores)+"]")
    count = sc.parallelize(range(0, samples)) \
            .filter(inside).count()
    sc.stop()

data = []
global cores
cores = 1
global samples
samples = N_SAMPLES_MIN
for cores in range(1,N_CORES_MAX-3):
    for samples in range(N_SAMPLES_MIN,N_SAMPLES_MAX,1000000):
        elapsed_time = timeit.timeit("code(cores, samples)", number=5, setup="from __main__ import code, cores, samples")/5
        print("{0} cores used. In {1} ".format(cores, elapsed_time))
        data.append([cores,samples,elapsed_time])

data = pandas.DataFrame(data)
data.to_csv("pi_acc.csv")
